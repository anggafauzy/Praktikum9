<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;

	class ArrayNotHasKeyTest extends TestCase {
		public function testFailure() {
			$this->assertArrayNotHasKey('foo', ['Inn' => 'Inn']);
		}
	}
?>
